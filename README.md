# Reporting System

This project consists of two main components: a Management Report, and an Hours Report. Each was developed to streamline the reporting processes and enhance data analysis for Risk International. The system leverages custom R-coded charts and tables to mirror industry-standard reports and provides dynamic management and time-tracking tools for internal use.

## 1. Management Report

This report is designed to provide Managing Directors with a high-level view of team performance and client engagement over the years. The system dynamically generates insights for year-over-year analysis and forecasting.

### Key Features:
- **Year-Over-Year View**: An overview of yearly client hours, allowing quick comparisons across time.
- **Dynamic Forecasting**: Built-in forecasting features predict year-end totals for each client based on current data.
- **Slicers and Filters**: Users can slice and filter data by different variables to tailor reports to specific teams or clients.

## 2. Hours Report

Developed from scratch, this report is based on a relational database containing a dozen tables and tens of thousands of rows in the fact table. It tracks time allocation and capacity management for teams and individuals within the company.

### Key Features:
- **Overview Page**: Provides top-level metrics such as total hours worked and forecasts across all projects.
- **Capacity Page**: Displays individual time availability, helping managers optimize resource allocation.
- **Projects Page**: Offers detailed views of specific projects, including hours logged, completion status, and projections.

---

This system has been instrumental in ensuring comprehensive, accurate reporting, and data-driven insights for Risk International’s policy and time management processes.

